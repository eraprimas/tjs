<?php

use vakata\database\Query;

session_start();

include("db/config.php");

// $sess = "asdasdasdasdasd";

if(isset($_SESSION["username"])){
    $resp = array();

    $sess = session_id();
    $username = $_SESSION["username"];
    $qty = intval($_POST['quantity']);
    $kodep = $_POST['kode'];
    $hrg = 0;
    $tipe = $_POST["tipe"];

    // Ngambil harga
    $sqlgetharga = "SELECT pls FROM master_price WHERE kode='$kodep'";
    $queryget = mysqli_query($conn, $sqlgetharga);
    $dataget = mysqli_fetch_array($queryget);
    $hrg = $dataget["pls"];
    if(!isset($hrg)) $hrg = 0;

    if ($tipe == "normal") {
        $sqlcek = "SELECT * FROM cart where user_id='$username' and kodep='$kodep'";
        $querycek = mysqli_query($conn, $sqlcek);
        $rowcount = mysqli_num_rows($querycek);
        $total = $qty * $hrg;

        $resp["totalan"] = $total;

        if ($rowcount < 1) {
            $sql = "INSERT INTO cart (user_id, sess_id, kodep)  VALUES ('$username','$sess','$kodep')";
            $query = mysqli_query($conn, $sql);

            $resp["rc1"] = $username . " & " . $sess . " & " . $kodep;

            $sql = "INSERT INTO cartdtl(sess_id, userid, kode_stok, jumlah, harga, total)  VALUES ('$sess','$username','$kodep', $qty, $hrg, $total)";
            $query = mysqli_query($conn, $sql);

            $resp["rc2"] = $sess . " & " . $username . " & " . $kodep . " & " . $qty . " & " . $hrg . " & " . $total;

        } else {
            $sql = "UPDATE cart SET kodep='$kodep' WHERE user_id='$username' AND sess_id='$sess'";
            $query = mysqli_query($conn, $sql);

            $resp["rce1"] = $username . " & " . $sess . " & " . $kodep;


            $sql = "UPDATE cartdtl SET total = total + ($hrg*$qty), jumlah = jumlah + $qty WHERE userid='$username' AND kode_stok='$kodep'";
            $query = mysqli_query($conn, $sql);

            $resp["rce2"] = $username . " & " . $hrg . " & " . $kodep . " & " . $qty;

        }
    } else if ($tipe == "indent") {
        $sqlcek = "SELECT * FROM icart where user_id='$username' and kodep='$kodep'";
        $querycek = mysqli_query($conn, $sqlcek);
        $rowcount = mysqli_num_rows($querycek);
        $total = $qty  * $hrg;

        if ($rowcount < 1) {
            $sql = "INSERT INTO icart(user_id,sess_id,kodep)  VALUES ('$username','$sess','$kodep')";
            $query = mysqli_query($conn, $sql);

            $sql = "INSERT INTO icartdtl(sess_id, userid, kode_stok, jumlah,harga, total)  VALUES ('$sess','$username','$kodep', $qty, $hrg, $total)";
            $query = mysqli_query($conn, $sql);
        } else {
            $sql = "UPDATE icart SET kodep='$kodep' WHERE user_id='$username' AND sess_id='$sess'";
            $query = mysqli_query($conn, $sql);

            $sql = "UPDATE icartdtl SET total = total + ($hrg*$qty), jumlah = jumlah + $qty WHERE userid='$username' AND kode_stok='$kodep'";
            $query = mysqli_query($conn, $sql);
        }
    }

    // if (!$query) {
    //     echo "Error di query ";
    // }
    // else{
    //     $resp["error"] = "ga masuk if blas";
    // }

    $totals = 0;
    $sqlupdate = "SELECT count(no) as 'jml' FROM cartdtl where userid='$username'";
    $queryupdate = mysqli_query($conn, $sqlupdate);

    if (!$queryupdate) {
        echo "Error di query pertama";
    }

    $row = mysqli_fetch_array($queryupdate);
    $resp["jmlN"] = $row['jml'];

    $totals = 0;
    $sqlupdate = "SELECT count(no) as 'jml' FROM icartdtl where userid='$username'";
    $queryupdate = mysqli_query($conn, $sqlupdate);

    if (!$queryupdate) {
        echo "Error di query kedua";
    }

    $row = mysqli_fetch_array($queryupdate);
    $resp["jmlI"] = $row['jml'];

    echo json_encode($resp);
}
else{
    $resp["msg"] = "notLogged";
    echo json_encode($resp);
}

?>
