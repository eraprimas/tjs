<?php
include('db/config.php');
include('rupiah.php');
?>

<!DOCTYPE html>
<html>
<style>
	html {
		scroll-behavior: smooth;
	}
</style>

<head>
	<title>Smart Marble</title>
	<?php include("./headerdkk/template-head.php"); ?>
</head>

<body class="homepages-1" id="realcontainer">
	<?php include('headerdkk/header.php'); ?>
	<div class="page-content">
		<!-- Slider Revolution Section -->
		<section class="home-slider style-home-slider-hp-1">
			<!-- the ID here will be used in the inline JavaScript below to initialize the slider -->
			<div id="rev_slider_1" class="rev_slider fullscreenbanner" data-version="5.4.5">
				<ul>
					<!-- BEGIN SLIDE 1 -->
					<li data-transition="boxslide">
						<!-- SLIDE'S MAIN BACKGROUND IMAGE -->
						<img src="resource/bravatgede.jpg" alt="slide-1" class="rev-slidebg">

						<!-- BEGIN LAYER -->
						<div class="tp-caption tp-resizeme slide-caption-title-1" data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:-20px;opacity:0;","ease":"Power3.easeInOut"}]' data-fontsize="['20', '25', '30', '35']" data-lineheight="['32', '35', '40', '45']" data-color="#d59f9f" data-textAlign="['center', 'center', 'center', 'center']" data-x="['center','center','center','center']" data-y="['middle','middle','middle','middle']" data-hoffset="['0', '0', '0', '0']" data-voffset="['-227', '-200', '-175', '-130']" data-width="['187', '250', '300', '350']" data-whitespace="normal" data-basealign="slide" data-responsive_offset="off">

						</div>
						<div class="tp-caption tp-resizeme slide-caption-title-2" style="margin-top: 450px;" data-frames='[{"delay":1100,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:-20px;opacity:0;","ease":"Power3.easeInOut"}]' data-fontsize="['90', '90', '80', '80']" data-lineheight="['70', '70', '60', '60']" data-color="#fff" data-textAlign="['center', 'center', 'center', 'center']" data-x="['center','center','center','center']" data-y="['middle','middle','middle','middle']" data-hoffset="['0', '0', '0', '0']" data-voffset="['-140', '-117', '-110', '-90']" data-width="['1200', '850', '850', '800']" data-whitespace="normal" data-basealign="slide" data-responsive_offset="off">
							<img width="40%" height="10%" src="resource/logomerk/bv.png"> <label style="color:black"> COLLECTION</label>
						</div>
						<div class="tp-caption tp-resizeme slide-caption-title-3" data-frames='[{"delay":0,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]' data-fontsize="['80', '80', '80', '90']" data-lineheight="['60', '60', '50', '50']" data-color="['#666','#fff','#fff','#fff']" data-textAlign="['center', 'center', 'center', 'center']" data-x="['right','right','right','right']" data-y="['bottom','bottom','bottom','bottom']" data-hoffset="['27', '18', '18', '60']" data-voffset="['28', '30', '30', '30']" data-width="['250', '250', '300', '350']" data-whitespace="normal" data-basealign="slide" data-responsive_offset="off">
							<label style="color:black">01<label>
						</div>
						<div class="tp-caption tp-resizeme slide-caption-title-3" data-frames='[{"delay":0,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]' data-fontsize="['13', '15', '20', '35']" data-lineheight="['32', '35', '40', '45']" data-color="['#666','#fff','#fff','#fff']" data-textAlign="['center', 'center', 'center', 'center']" data-x="['right','right','right','right']" data-y="['bottom','bottom','bottom','bottom']" data-hoffset="['14', '-23', '-20', '35']" data-voffset="['63', '56', '50', '37']" data-width="['187', '250', '300', '350']" data-whitespace="normal" data-basealign="slide" data-responsive_offset="off">
							<label style="color:black">/<label>
						</div>
						<div class="tp-caption tp-resizeme slide-caption-title-3" data-frames='[{"delay":0,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]' data-fontsize="['20', '25', '30', '40']" data-lineheight="['32', '35', '40', '45']" data-color="['#666','#fff','#fff','#fff']" data-textAlign="['center', 'center', 'center', 'center']" data-x="['right','right','right','right']" data-y="['bottom','bottom','bottom','bottom']" data-hoffset="['-6', '-43', '-40', '15']" data-voffset="['63', '56', '50', '37']" data-width="['187', '250', '300', '350']" data-whitespace="normal" data-basealign="slide" data-responsive_offset="off">
							<label style="color:black">02<label>
						</div>
						<!-- END LAYER -->
					</li>
					<!-- END SLIDE 1 -->

					<!-- BEGIN SLIDE 2 -->
					<li data-transition="boxslide">
						<!-- SLIDE'S MAIN BACKGROUND IMAGE -->
						<img src="resource/simpologede.jpg" alt="slide-2" class="rev-slidebg">

						<!-- BEGIN LAYER -->
						<div class="tp-caption tp-resizeme slide-caption-title-1" data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:-20px;opacity:0;","ease":"Power3.easeInOut"}]' data-fontsize="['20', '25', '30', '35']" data-lineheight="['32', '35', '40', '45']" data-color="#3d4552" data-textAlign="['center', 'center', 'center', 'center']" data-x="['center','center','center','center']" data-y="['middle','middle','middle','middle']" data-hoffset="['0', '0', '0', '0']" data-voffset="['-227', '-200', '-175', '-130']" data-width="['187', '250', '300', '350']" data-whitespace="normal" data-basealign="slide" data-responsive_offset="off">
							PREMIUM
						</div>
						<div class="tp-caption tp-resizeme slide-caption-title-2" data-frames='[{"delay":1000,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:-20px;opacity:0;","ease":"Power3.easeInOut"}]' data-fontsize="['90', '90', '80', '80']" data-lineheight="['70', '70', '60', '60']" data-color="#fff" data-textAlign="['center', 'center', 'center', 'center']" data-x="['center','center','center','center']" data-y="['middle','middle','middle','middle']" data-hoffset="['0', '0', '0', '0']" data-voffset="['-140', '-117', '-110', '-90']" data-width="['1200', '850', '850', '800']" data-whitespace="normal" data-basealign="slide" data-responsive_offset="off">
							<span>SIMPOLO</span> CERAMICS
						</div>
						<div class="tp-caption tp-resizeme slide-caption-title-3" data-frames='[{"delay":0,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]' data-fontsize="['80', '80', '80', '90']" data-lineheight="['60', '60', '50', '50']" data-color="['#666','#fff','#fff','#fff']" data-textAlign="['center', 'center', 'center', 'center']" data-x="['right','right','right','right']" data-y="['bottom','bottom','bottom','bottom']" data-hoffset="['27', '27', '27', '60']" data-voffset="['28', '30', '30', '30']" data-width="['250', '250', '300', '350']" data-whitespace="normal" data-basealign="slide" data-responsive_offset="off">
							<label style="color:black">02<label>
						</div>
						<div class="tp-caption tp-resizeme slide-caption-title-3" data-frames='[{"delay":0,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]' data-fontsize="['13', '15', '20', '35']" data-lineheight="['32', '35', '40', '45']" data-color="['#666','#fff','#fff','#fff']" data-textAlign="['center', 'center', 'center', 'center']" data-x="['right','right','right','right']" data-y="['bottom','bottom','bottom','bottom']" data-hoffset="['3', '-23', '-20', '32']" data-voffset="['63', '56', '50', '37']" data-width="['187', '250', '300', '350']" data-whitespace="normal" data-basealign="slide" data-responsive_offset="off">
							<label style="color:black">/<label>
						</div>
						<div class="tp-caption tp-resizeme slide-caption-title-3" data-frames='[{"delay":0,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]' data-fontsize="['20', '25', '30', '40']" data-lineheight="['32', '35', '40', '45']" data-color="['#666','#fff','#fff','#fff']" data-textAlign="['center', 'center', 'center', 'center']" data-x="['right','right','right','right']" data-y="['bottom','bottom','bottom','bottom']" data-hoffset="['-17', '-43', '-40', '15']" data-voffset="['63', '56', '50', '37']" data-width="['187', '250', '300', '350']" data-whitespace="normal" data-basealign="slide" data-responsive_offset="off">
							<label style="color:black">02<label>
						</div>
						<!-- END LAYER -->
					</li>
					<!-- END SLIDE 2 -->


				</ul>
			</div>
		</section>
		<!-- End Slider Revolution Section -->

		<!-- Categories Section -->
		<section class="categories-hp-1 section-box">
			<div class="container">
				<div class="categories-content">
					<div class="row">
						<!-- Categories 1 -->
						<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
							<div class="categories-detail lighting">
								<a href="shop-full-width.html" class="images"><img src="resource/simpolostandar.jpg" alt="Lighting"></a>
								<div class="product">
									<a href="shop-full-width.html">
										<span class="name">
											<span class="line">- </span>
											SIMPOLO
										</span>
										<span class="quantity">- 12 Products</span>
									</a>
								</div>
							</div>
						</div>
						<!-- Categories 2 -->
						<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
							<div class="categories-detail furniture">
								<a href="shop-full-width.html" class="images"><img src="resource/kohlerrodokgede.JPG" alt="Furniture"></a>
								<div class="product">
									<a href="shop-full-width.html">
										<span class="name">
											<span class="line">- </span>
											KOHLER
										</span>
										<span class="quantity">- 15 Products</span>
									</a>
								</div>
							</div>
						</div>
						<!-- Categories 3 -->
						<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
							<div class="categories-detail decoration">
								<a href="shop-full-width.html" class="images"><img src="resource/bravatstandar.jpg" alt="Decoration"></a>
								<div class="product">
									<a href="shop-full-width.html">
										<span class="name">
											<span class="line">- </span>
											BRAVAT
										</span>
										<span class="quantity">- 20 Products</span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Categories Section -->

		<!-- Featured Sale Section -->
		<div id="kontainerAnjay">

		</div>
		<!-- End Of Product -->

		<!-- Banner Section -->
		<section class="banner-hp-1 section-box">
			<div class="container">
				<div class="banner-content">
					<div class="row">
						<div class="col-xl-4 col-lg-4 col-md-4 cl-sm-12 col-12">
							<div class="banner-left">
								<a href="#" class="bg-image">
									<img src="resource/sale.jpg" alt="banner">
								</a>
								<div class="info">
									<a href="#" class="sale">SALE UP TO</a>
									<span>50% OFF</span>
									<a href="shop-full-width.html" class="shop">Shop Now<i class="zmdi zmdi-arrow-right"></i></a>
								</div>
							</div>
						</div>
						<div class="col-xl-8 col-lg-8 col-md-8 cl-sm-12 col-12">
							<div class="banner-right">
								<a href="#" class="bg-image">
									<img src="resource/wulan.jpg" alt="banner">
								</a>
								<div class="info" style="margin-left: 100px;">
									<p class="text-1">Get <span>30%</span> Off</p>
									<p class="text-2">on Elisa Armchair collection</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Banner Section -->

		<!-- Insta Follow Section -->
		<section class="insta-hp-1 section-box">
			<div class="container">
				<div class="insta-content">
					<h2>Our Brands</h2>
					<span></span>
					<div class="row">
						<!-- Insta 1 -->
						<div class="col">
							<div class="insta-detail">
								<a href="" class="insta-image">
									<img src="../venturafe1/images/clients/logo/kohler.jpg" alt="insta-1">
									<div class="overlay"></div>
								</a>
							</div>
						</div>
						<!-- Insta 2 -->
						<div class="col">
							<div class="insta-detail">
								<a href="#" class="insta-image">
									<img src="../venturafe1/images/clients/logo/fiandre.png" alt="insta-2">
									<div class="overlay"></div>
								</a>
							</div>
						</div>
						<!-- Insta 3 -->
						<div class="col">
							<div class="insta-detail">
								<a href="#" class="insta-image">
									<img src="../venturafe1/images/clients/logo/bri.png" alt="insta-3">
									<div class="overlay"></div>
								</a>
							</div>
						</div>
						<!-- Insta 4 -->
						<div class="col">
							<div class="insta-detail">
								<a href="#" class="insta-image">
									<img src="../venturafe1/images/clients/logo/bra.png" alt="insta-4">
									<div class="overlay"></div>
								</a>
							</div>
						</div>
						<!-- Insta 5 -->
						<div class="col">
							<div class="insta-detail">
								<a href="#" class="insta-image">
									<img src="../venturafe1/images/clients/logo/tre.png" alt="insta-5">
									<div class="overlay"></div>
								</a>
							</div>
						</div>
						<!-- Insta 6 -->
						<div class="col">
							<div class="insta-detail">
								<a href="#" class="insta-image">
									<img src="../venturafe1/images/clients/logo/sm.png" alt="insta-5">
									<div class="overlay"></div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Brand -->

		<section class="featured-hp-1">
			<div class="container">
				<div class="featured-content woocommerce">
					<span></span>
					<div class="row">
						<div class="col borderRight">
							<div class="insta-detail">
								<a href="#" class="insta-image">
									<h1><i class="fa fa-thumbs-o-up"></i></h1>
									<h4 style="font-weight:bold">100% ORIGINAL</h4>
									<h5>We guarantee you the sale of Original Brands.</h5>
									<div class="overlay"></div>
								</a>
							</div>
						</div>
						<div class="col borderRight">
							<div class="insta-detail">
								<a href="#" class="insta-image">
									<h1><i class="fa fa-credit-card"></i></h1>
									<h4 style="font-weight:bold">PAYMENT OPTIONS</h4>
									<h5>We accept Bank Transfer, Debit, Visa, and MasterCard.</h5>
									<div class="overlay"></div>
								</a>
							</div>
						</div>
						<div class="col borderRight">
							<div class="insta-detail">
								<a href="#" class="insta-image">
									<h1><i class="fa fa-truck"></i></h1>
									<h4 style="font-weight:bold">FREE DELIVERY</h4>
									<h5>Free Delivery for Jakarta & Surabaya</h5>
									<div class="overlay"></div>
								</a>
							</div>
						</div>
						<div class="col borderRight">
							<div class="insta-detail">
								<a href="#" class="insta-image">
									<h1><i class="fa fa-undo"></i></h1>
									<h4 style="font-weight:bold">14-DAYS RETURNS</h4>
									<h5>Within 14 days in same condition as received</h5>
									<div class="overlay"></div>
								</a>
							</div>
						</div>
						<div class="col borderRight">
							<div class="insta-detail">
								<a href="#" class="insta-image">
									<h1><i class="fa fa-home"></i></h1>
									<h4 style="font-weight:bold">WARRANTY</h4>
									<h5>3 years for Sanitary ware & 2 years for tiles</h5>
									<div class="overlay"></div>
								</a>
							</div>
						</div>
						<div class="col borderRight">
							<div class="insta-detail">
								<a href="#" class="insta-image">
									<h1><i class="fa fa-upload"></i></h1>
									<h4 style="font-weight:bold">PRE & POST SALES SERVICES</h4>
									<h5>Design Consultancy, Installation, Supervision, Maintenance</h5>
									<div class="overlay"></div>
								</a>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
	</div>
	<br>
	<br>
	<br>

	<?php include("headerdkk/footer.php"); ?>
	<script type="text/javascript">
		function scrollToAnchor(aid) {
			var aTag = $("div[id='" + aid + "']");
			$('html,body').animate({
				scrollTop: aTag.offset().top
			}, 'slow');
		}

		// $.ajax({
		// 	type: 'POST',
		// 	url: 'ajaxHome.php',
		// 	success: function(data) {
		// 		document.getElementById("kontainerAnjay").innerHTML = data;
		// 		$('.images-preloader').fadeOut();
		// 	}
		// });
		$("#kontainerAnjay").load("ajaxHome.php", {}, function(res, status, xhr) {
			if (location.hash !== "undefined") {
				scrollToAnchor(location.hash.substring(1));
			}
		});
	</script>
</body>

</html>